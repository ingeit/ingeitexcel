import express from 'express';
import cors from 'cors';
import path from 'path';
import XLSX from 'xlsx';
const port = 9579; // 'XLSX'
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.options('*', cors())
app.use('/api');

const make_book = data => {
	const ws = XLSX.utils.aoa_to_sheet(data);
	const wb = XLSX.utils.book_new();
	XLSX.utils.book_append_sheet(wb, ws, "SheetJS");
	return wb;
}

app.post('/readFile', function ({ body: { file } }, res) {

	const excel = xlsx.read(file, { type: 'base64' });
	const data = xlsx.utils.sheet_to_json(excel.Sheets[excel.SheetNames[0]], { header: 1,defval: '' });
	const headers = data.shift(); // eliminamos el primer elemento del array que es la fila con el nombre de las columnas del excel para luego usarlo para armar el objeto
	const _arrayOBJ = data.map(row => {
		const newRow = {};
		row.map((celda, index) => { newRow[headers[index]] = celda });
		return newRow;
	});
	res.json(_arrayOBJ);
});

app.post('/getFile', function ({ body: { data } }, res) {
	const wb = make_book(JSON.parse(data));
	const file = XLSX.write(wb, { type: 'buffer', bookType: 'xlsx' });
	res.status(200).send(file);
	// res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	// res.setHeader("Content-Disposition", "attachment;filename=" + "Operaciones.xlsx");
	// res.end(file, 'binary');
});

app.listen(port, function () { console.log('Serving HTTP on port ' + port); });