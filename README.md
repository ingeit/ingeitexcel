# IngeitExcel

IngeitExcel cuenta con dos metodos, uno para mandarle la data y que retone el excel, y otro para que se envie el file, y retorne los datos.

    post: localhost:9579/api/excel

**headers**

    Content-Type: application/json
**body**

    {
    	"to": "masterk63@gmail.com",
    	"subject": "test",
    	"template":"default",//nombre del template, cambia segun el template a usar.
    	"context": {
    		"data": [
    		    { "username": "kevin", "email": "gomee" }
    		  ]
    	}
    }
